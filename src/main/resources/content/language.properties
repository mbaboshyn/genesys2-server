#-------------------------------------------------------------------------------
# Copyright 2014 Global Crop Diversity Trust
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

# Errors
http-error.401=Unauthorized
http-error.401.text=Authentication is required.
http-error.403=Access denied
http-error.403.text=You do not have permission to access the resource.
http-error.404=Not found
http-error.404.text=The requested resource could not be found but may be available again in the future.
http-error.500=Internal Server Error
http-error.500.text=An unexpected condition was encountered and no more specific message is suitable.
http-error.503=Service Unavailable
http-error.503.text=The server is currently unavailable (because it is overloaded or down for maintenance).


# Login
login.username=Username
login.password=Password
login.invalid-credentials=Invalid credentials.
login.remember-me=Remember me
login.login-button=Login
login.register-now=Create an account
logout=Logout
login.forgot-password=Forgot password
login.with-google-plus=Login with Google+

# Registration
registration.page.title=Create a user account
registration.title=Create your account
registration.invalid-credentials=Invalid credentials.
registration.user-exists=Username already taken.
registration.email=E-mail
registration.password=Password
registration.confirm-password=Repeat password
registration.full-name=Your full name
registration.create-account=Create account
captcha.text=Captcha text


id=ID

name=Name
description=Description
actions=Actions
add=Add
edit=Edit
save=Save
create=Create
cancel=Cancel
delete=Delete

jump-to-top=Back to top!

pagination.next-page=Next >
pagination.previous-page=< Previous


# Language
locale.language.change=Change locale
i18n.content-not-translated=This content is not available in your language. Please contact us if you could help to translate it!

data.error.404=The data you requested was not found in the system.
page.rendertime=Processing this page took {0}ms.

footer.copyright-statement=&copy; 2013 Data Providers and GCDT

menu.home=Home
menu.browse=Browse
menu.datasets=C&E Data
menu.descriptors=Descriptors
menu.countries=Countries
menu.institutes=Institutes
menu.my-list=My List
menu.about=About
menu.contact=Contact
menu.disclaimer=Disclaimer
menu.feedback=Feedback
menu.help=Help
menu.terms=Terms and Conditions of Use
menu.copying=Copyright policy
menu.privacy=Privacy policy

page.home.title=Genesys PGR

user.pulldown.administration=Administration
user.pulldown.users=User list
user.pulldown.logout=Logout
user.pulldown.profile=View profile
user.pulldown.oauth-clients=OAuth clients
user.pulldown.teams=Teams

user.pulldown.heading={0}
user.create-new-account=Create an account
user.full-name=Full Name
user.email=E-mail Address
user.account-status=Account Status
user.account-disabled=Account disabled
user.account-locked-until=Account locked until
user.roles=User roles
userprofile.page.title=User profile
userprofile.update.title=Update your profile

user.page.list.title=Registered user accounts

crop.croplist=Crop list
crop.all-crops=All crops
crop.page.profile.title={0} profile
crop.taxonomy-rules=Taxonomic rules
crop.view-descriptors=View crop descriptors...

activity.recent-activity=Recent activity

country.page.profile.title=Country profile: {0}
country.page.list.title=Country list
country.page.not-current=This is a historic entry.
country.page.faoInstitutes={0} institutes registered in WIEWS
country.stat.countByLocation={0} accessions at institutes in this country
country.stat.countByOrigin={0} accessions registered in Genesys come from this country.
country.statistics=Country statistics
country.accessions.from=Accessions collected in {0}
country.more-information=More information:
country.replaced-by=Country code is replaced by: {0}
country.is-itpgrfa-contractingParty={0} is party to the International Treaty on Plant Genetic Resources for Food and Agriculture (ITPGRFA).
select-country=Select country

faoInstitutes.page.list.title=WIEWS Institutes
faoInstitutes.page.profile.title=WIEWS {0}
faoInstitutes.stat.accessionCount=Accessions in Genesys: {0}.
faoInstitutes.stat.datasetCount=Additional datasets in Genesys: {0}.
faoInstitute.stat-by-crop=Most represented Crops
faoInstitute.stat-by-genus=Most represented Genera
faoInstitute.stat-by-species=Most represented Species
faoInstitute.accessionCount={0} accessions
faoInstitute.statistics=Institute statistics
faoInstitutes.page.data.title=Accessions in {0}
faoInstitute.accessions.at=Accessions at {0}
faoInstitutes.viewAll=View all registered institutes
faoInstitutes.viewActiveOnly=View institutes with accessions in Genesys
faoInstitute.no-accessions-registered=Please get in touch with us if you can facilitate in providing data from this institiute.
faoInstitute.institute-not-current=This record is archived.
faoInstitute.view-current-institute=Navigate to {0}.
faoInstitute.country=Country
faoInstitute.code=WIEWS Code
faoInstitute.email=Contact email
faoInstitute.acronym=Acronym
faoInstitute.url=Web link
faoInstitute.member-of-organizations-and-networks=Organizations and Networks:
faoInstitute.uniqueAcceNumbs.true=Each accession number is unique within this institute.
faoInstitute.uniqueAcceNumbs.false=The same accession number may be used in separate collections in this institute.
faoInstitute.requests.mailto=Email address for material requests:
faoInstitute.allow.requests=Allow material requests
faoInstitute.sgsv=Code SGSV
faoInstitute.data-title=Database of information on accessions maintained at {0}
faoInstitute.overview-title=Overview of information on accessions maintained at {0}
faoInstitute.datasets-title=Characterization and evaluation data provided by {0}
faoInstitute.meta-description=Overview of plant genetic resources maintained in collections at {0} ({1}) genebank
faoInstitute.link-species-data=View information on {2} accessions at {0} ({1})
faoInstitute.wiewsLink={0} details on FAO WIEWS website

view.accessions=Browse accessions
view.datasets=View datasets
paged.pageOfPages=Page {0} of {1}
paged.totalElements={0} entries

accessions.number={0} accessions
accession.metadatas=C&E Data
accession.methods=Characterization & Evaluation data
unit-of-measure=Unit of Measure

ce.trait=Trait
ce.sameAs=Same As
ce.methods=Methods
ce.method=Method
method.fieldName=DB Field

accession.uuid=UUID
accession.accessionName=Accession number
accession.origin=Country of origin
accession.holdingInstitute=Holding institute
accession.holdingCountry=Location
accession.taxonomy=Scientific name
accession.crop=Crop name
accession.otherNames=Also known as
accession.inTrust=In Trust
accession.mlsStatus=MLS Status
accession.duplSite=Safety duplication institute
accession.inSvalbard=Safety duplicated in Svalbard
accession.inTrust.true=This accession is under Article 15 of the International Treaty on Plant Genetic Resources for Food and Agriculture.
accession.mlsStatus.true=This accession is in the Multilateral System of the ITPGRFA. 
accession.inSvalbard.true=Safety duplicated in Svalbard Global Seed Vault.
accession.not-available-for-distribution=The accession is NOT available for distribution.
accession.this-is-a-historic-entry=This is a historic record of an accession.
accession.historic=Historic entry
accession.available-for-distribution=The accession is available for distribution. 
accession.elevation=Elevation
accession.geolocation=Geolocation (lat, long)

accession.storage=Type of Germplasm storage
accession.storage.=
accession.storage.10=Seed collection
accession.storage.11=Short term seed collection
accession.storage.12=Medium term seed collection
accession.storage.13=Long term seed collection
accession.storage.20=Field collection
accession.storage.30=In vitro collection
accession.storage.40=Cryopreserved collection
accession.storage.50=DNA collection
accession.storage.99=Other

accession.breeding=Breeder information
accession.breederCode=Breeder Code
accession.pedigree=Pedigree
accession.collecting=Collecting information
accession.collecting.site=Location of collecting site
accession.collecting.institute=Collecting institute
accession.collecting.number=Collecting number
accession.collecting.date=Collecting date of sample
accession.collecting.mission=Collecting mission ID
accession.collecting.source=Collecting/Acquisition source

accession.collectingSource.=
accession.collectingSource.10=Wild habitat
accession.collectingSource.11=Forest or woodland
accession.collectingSource.12=Shrubland
accession.collectingSource.13=Grassland
accession.collectingSource.14=Desert or tundra
accession.collectingSource.15=Aquatic habitat
accession.collectingSource.20=Field or cultivated habitat
accession.collectingSource.21=Field
accession.collectingSource.22=Orchard
accession.collectingSource.23=Backyard, kitchen or home garden (urban, peri-urban or rural)
accession.collectingSource.24=Fallow land
accession.collectingSource.25=Pasture
accession.collectingSource.26=Farm store
accession.collectingSource.27=Threshing floor
accession.collectingSource.28=Park
accession.collectingSource.30=Market or shop
accession.collectingSource.40=Institute, Experimental station, Research organization, Genebank
accession.collectingSource.50=Seed company
accession.collectingSource.60=Weedy, disturbed or ruderal habitat
accession.collectingSource.61=Roadside
accession.collectingSource.62=Field margin
accession.collectingSource.99=Other

accession.donor.institute=Donor institute
accession.donor.accessionNumber=Donor accession ID
accession.geo=Geographic information
accession.geo.datum=Coordinate datum
accession.geo.method=Georeferencing method
accession.geo.uncertainty=Coordinate uncertainty
accession.sampleStatus=Biological status of accession

accession.sampleStatus.=
accession.sampleStatus.100=Wild
accession.sampleStatus.110=Natural
accession.sampleStatus.120=Semi-natural/wild
accession.sampleStatus.130=Semi-natural/sown
accession.sampleStatus.200=Weedy
accession.sampleStatus.300=Traditional cultivar/Landrace
accession.sampleStatus.400=Breeding/Research Material
accession.sampleStatus.410=Breeders Line
accession.sampleStatus.411=Synthetic population
accession.sampleStatus.412=Hybrid
accession.sampleStatus.413=Founder stock/base population
accession.sampleStatus.414=Inbred line
accession.sampleStatus.415=Segregating population
accession.sampleStatus.416=Clonal selection
accession.sampleStatus.420=Genetic stock
accession.sampleStatus.421=Mutant
accession.sampleStatus.422=Cytogenetic stocks
accession.sampleStatus.423=Other genetic stocks
accession.sampleStatus.500=Advanced/improved cultivar
accession.sampleStatus.600=GMO
accession.sampleStatus.999=Other

accession.availability=Availability for distribution
accession.aliasType.ACCENAME=Accession name
accession.aliasType.DONORNUMB=Donor accession identifier
accession.aliasType.BREDNUMB=Name assigned by breeder
accession.aliasType.COLLNUMB=Collecting number
accession.aliasType.OTHERNUMB=Other names
accession.aliasType.DATABASEID=(Database ID)
accession.aliasType.LOCALNAME=(Local name)

accession.availability.=Unknown
accession.availability.true=Available
accession.availability.false=Not available

accession.historic.true=Historic
accession.historic.false=Active
accession.acceUrl=Additional accession URL

accession.page.profile.title=Accession profile: {0}
accession.page.resolve.title=Multiple accessions found
accession.resolve=Multiple accessions with the name ''{0}'' found in Genesys. Select one from the list. 
accession.page.data.title=Accession browser
accession.taxonomy-at-institute=View {0} at {1}

accession.svalbard-data=Svalbard Global Seed Vault duplication data
accession.svalbard-data.taxonomy=Reported scientific name
accession.svalbard-data.depositDate=Deposit date
accession.svalbard-data.boxNumber=Box number
accession.svalbard-data.quantity=Quantity
accession.remarks=Remarks

taxonomy.genus=Genus
taxonomy.species=Species
taxonomy.taxonName=Scientific name
taxonomy-list=Taxonomy list

selection.page.title=Selected accessions
selection.add=Add {0} to list
selection.remove=Remove {0} from list
selection.clear=Clear the list
selection.empty-list-warning=You have not added any accessions to the list.
selection.add-many=Check and add
selection.add-many.accessionIds=List accession IDs as used in Genesys separated by space or new line.

savedmaps=Remember current map
savedmaps.list=Map list
savedmaps.save=Remember map

filter.enter.title=Enter filter title
filters.page.title=Data filters
filters.view=Current filters
filter.filters-applied=You have applied filters.
filter.filters-not-applied=You can filter the data.
filters.data-is-filtered=The data is filtered.
filters.toggle-filters=Filters
filter.taxonomy=Scientific name
filter.art15=ITPGRFA Art. 15 accession
filter.acceNumb=Accession number
filter.alias=Accession name
filter.crops=Crop name
filter.orgCty.iso3=Country of Origin
filter.institute.code=Holding Institute name
filter.institute.country.iso3=Country of holding institute
filter.sampStat=Biological status of accession
filter.institute.code=Holding Institute
filter.geo.latitude=Latitude
filter.geo.longitude=Longitude
filter.geo.elevation=Elevation
filter.taxonomy.genus=Genus
filter.taxonomy.species=Species
filter.taxonomy.subtaxa=Subtaxa
filter.taxSpecies=Species
filter.taxonomy.sciName=Scientific name
filter.sgsv=Safety duplicated in Svalbard
filter.mlsStatus=MLS status of the accession
filter.available=Available for distribution
filter.historic=Historic record
filter.donorCode=Donor institute
filter.duplSite=Site of safety duplication
filter.download-dwca=Download ZIP
filter.download-mcpd=Download MCPD
filter.add=Add filter
filter.additional=Additional filters
filter.apply=Apply
filter.close=Close
filter.remove=Remove filter
filter.autocomplete-placeholder=Type more than 3 characters...
filter.coll.collMissId=Collecting mission ID 
filter.storage=Type of Germplasm storage
filter.string.equals=Equals
filter.string.like=Starts with
filter.inverse=Excluding
filter.set-inverse=Exclude selected values


search.page.title=Full-text Search
search.no-results=No matches found for your query.
search.input.placeholder=Search Genesys...
search.search-query-missing=Please enter your search query.
search.search-query-failed=Sorry, search failed with error {0}
search.button.label=Search
search.add-genesys-opensearch=Register Genesys Search with your browser

admin.page.title=Genesys 2 Administration
metadata.page.title=Datasets
metadata.page.view.title=Dataset details
metadata.download-dwca=Download ZIP
page.login=Login

traits.page.title=Descriptors
trait-list=Descriptors


organization.page.list.title=Organizations and Networks
organization.page.profile.title={0}
organization.slug=Organization or Network acronym
organization.title=Full name
filter.institute.networks=Network


menu.report-an-issue=Report an issue
menu.scm=Source code
menu.translate=Translate Genesys

article.edit-article=Editing article
article.slug=Article slug (URL)
article.title=Article title
article.body=Article body
article.post-to-transifex=Post to Transifex
article.remove-from-transifex=Remove from Transifex
article.transifex-resource-updated=The resource was successfully updated on Transifex.
article.transifex-resource-removed=The resource was successfully removed from Transifex.
article.transifex-failed=An error occured while exchanging data with Transifex

activitypost=Activity post
activitypost.add-new-post=Add new post
activitypost.post-title=Post title
activitypost.post-body=Body

blurp.admin-no-blurp-here=No blurp here.
blurp.blurp-title=Blurp title
blurp.blurp-body=Blurp contents
blurp.update-blurp=Save blurp


oauth2.confirm-request=Confirm access
oauth2.confirm-client=You, <b>{0}</b>, hereby authorize <b>{1}</b> to access your protected resources.
oauth2.button-approve=Yes, allow access
oauth2.button-deny=No, deny access

oauth2.authorization-code=Authorization code
oauth2.authorization-code-instructions=Copy this authorization code: 

oauth2.access-denied=Access denied
oauth2.access-denied-text=You have denied access to your resources.

oauth-client.page.list.title=OAuth2 Clients
oauth-client.page.profile.title=OAuth2 Client: {0}
oauth-client.active-tokens=List of tokens issued
client.details.title=Client Title
client.details.description=Description

maps.loading-map=Loading map...
maps.view-map=View map
maps.accession-map=Accession map

audit.createdBy=Created by {0}
audit.lastModifiedBy=Last updated by {0}

itpgrfa.page.list.title=Parties to the ITPGRFA

request.page.title=Requesting material from holding institutes
request.total-vs-available=Out of {0} accessions listed, {1} are known to be available for distribution.
request.start-request=Request available germplasm
request.your-email=Your E-mail address:
request.accept-smta=SMTA/MTA acceptance:
request.smta-will-accept=I will accept the terms and conditions of SMTA/MTA
request.smta-will-not-accept=I will NOT accept the terms and conditions of SMTA/MTA
request.smta-not-accepted=You did not indicate that you will accept the terms and conditions of SMTA/MTA. Your request for material will not be processed.
request.purpose=Specify use of material:
request.purpose.0=Other (please elaborate in Notes field)
request.purpose.1=Research for food and agriculture
request.notes=Provide any additional comments and notes:
request.validate-request=Validate your request
request.confirm-request=Confirm receipt of request
request.validation-key=Validation key:
request.button-validate=Validate
validate.no-such-key=Validation key is not valid.

team.user-teams=User's Teams
team.create-new-team=Create a new team
team.team-name=Team name
team.leave-team=Leave team
team.team-members=Team members
team.page.profile.title=Team: {0}
team.page.list.title=All teams
validate.email.key=Enter key
validate.email=Email validation
validate.email.invalid.key=Invalid key

edit-acl=Edit permissions
acl.page.permission-manager=Permission Manager
acl.sid=Security Identity
acl.owner=Object Owner
acl.permission.1=Read
acl.permission.2=Write
acl.permission.4=Create
acl.permission.8=Delete
acl.permission.16=Manage


ga.tracker-code=GA Tracker Code

boolean.true=Yes
boolean.false=No
boolean.null=Unknown
userprofile.password=Reset password
userprofile.enter.email=Enter your email
userprofile.enter.password=Enter new password
userprofile.email.send=Send email

verification.invalid-key=Token key is not valid.
verification.token-key=Validation key
login.invalid-token=Invalid access token

descriptor.category=Descriptor category
method.coding-table=Coding table

oauth-client.info=Client info
oauth-client.list=List of oauth clients
client.details.client.id=Client details id
client.details.additional.info=Additional info
client.details.token.list=List of tokens
client.details.refresh-token.list=List of refresh tokens
oauth-client.remove=Remove
oauth-client.remove.all=Remove all
oauth-client=Client
oauth-client.token.issue.date=Issue date
oauth-client.expires.date=Expires date
oauth-client.issued.tokens=Issued tokens
client.details.add=Add OAuth Client
oauth-client.create=Create OAuth Client
oauth-client.id=Client ID
oauth-client.secret=Client Secret
oauth-client.redirect.uri=Client redirect URI
oauth-client.access-token.accessTokenValiditySeconds=Access token validity
oauth-client.access-token.refreshTokenValiditySeconds=Refresh token validity
oauth-client.access-token.defaultDuration=Use default
oauth-client.title=Client title
oauth-client.description=Description
oauth2.error.invalid_client=Invalid Client ID. Validate your client_id and client_secret parameters.

team.user.enter.email=Enter user email
user.not.found=User not found
team.profile.update.title=Update team information

autocomplete.genus=Find Genus...

stats.number-of-countries={0} Countries
stats.number-of-institutes={0} Institutes
stats.number-of-accessions={0} Accessions

navigate.back=\u21E0 Back


content.page.list.title=Article list
article.lang=Language
article.classPk=Object

session.expiry-warning-title=Session expiration warning
session.expiry-warning=Your current session is about to expire. Do you wish to extend this session?
session.expiry-extend=Extend session

download.kml=Download KML

data-overview=Statistical overview
data-overview.short=Overview
data-overview.institutes=Holding institutes
data-overview.composition=Composition of genebank holdings
data-overview.sources=Sources of material
data-overview.management=Collection management
data-overview.availability=Availability of material
data-overview.otherCount=Other
data-overview.missingCount=Not specified
data-overview.totalCount=Total
data-overview.donorCode=FAO WIEWS code of donor institute
data-overview.mlsStatus=Available for distribution under the MLS


admin.cache.page.title=Application Cache Overview
cache.stat.map.ownedEntryCount=Owned cache entries
cache.stat.map.lockedEntryCount=Locked cache entries
cache.stat.map.puts=Number of writes to cache
cache.stat.map.hits=Number of cache hits

loggers.list.page=List of configured loggers
logger.add-logger=Add logger
logger.edit.page=Logger configuration page
logger.name=Logger name
logger.log-level=Log level
logger.appenders=Log appenders

menu.admin.index=Admin
menu.admin.loggers=Loggers
menu.admin.caches=Caches
menu.admin.ds2=DS2 Datasets

worldclim.monthly.title=Climate at collecting site
worldclim.monthly.precipitation.title=Monthly precipitation
worldclim.monthly.temperatures.title=Monthly temperatures
worldclim.monthly.precipitation=Monthly precipitation [mm]
worldclim.monthly.tempMin=Minimum temperature [°C]
worldclim.monthly.tempMean=Mean temperature [°C]
worldclim.monthly.tempMax=Maximum temperature [°C]

worldclim.other-climate=Other climatic data

download.page.title=Before you download
download.download-now=Start download, I will wait.

resolver.page.index.title=Find permanent identifier
resolver.acceNumb=Accession Number
resolver.instCode=Holding institute code
resolver.lookup=Find identifier
resolver.uuid=UUID
resolver.resolve=Resolve

resolver.page.reverse.title=Resolution results
accession.purl=Permanent URL

menu.admin.kpi=KPI
admin.kpi.index.page=KPI
admin.kpi.execution.page=KPI Execution
admin.kpi.executionrun.page=Execution run details

accession.pdci=Passport Data Completeness Index
accession.pdci.jumbo=PDCI Score: {0,number,0.00} of 10.0
accession.pdci.institute-avg=Average PDCI score for this institute is {0,number,0.00}
accession.pdci.about-link=Read about Passport Data Completeness Index
accession.pdci.independent-items=Independent of the population type
accession.pdci.dependent-items=Depending on the population type
accession.pdci.stats-text=Average PDCI score for {0} accessions is {1,number,0.00}, with minimum score of {2,number,0.00} and maximum score of {3,number,0.00}.

accession.donorNumb=Donor institute code
accession.acqDate=Acquisition date

accession.svalbard-data.url=Svalbard database URL
accession.svalbard-data.url-title=Deposit information in SGSV database 
accession.svalbard-data.url-text=View SGSV deposit information for {0}

filter.download-pdci=Download PDCI data
statistics.phenotypic.stats-text=Of the {0} accessions, {1} accessions ({2,number,0.##%}) have at least one additional trait recorded in a dataset available on Genesys (on average {3,number,0.##} traits in {4,number,0.##} datasets).
