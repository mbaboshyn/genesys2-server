/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.json;

import java.util.UUID;

import org.genesys2.server.model.json.Api1Constants.Accession;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccessionJson {
	private long version = 0;
	private Long genesysId;

	@JsonProperty(value = Accession.INSTCODE)
	private String instCode;
	@JsonProperty(value = Accession.ACCENUMB)
	private String acceNumb;
	@JsonProperty(value = Accession.GENUS)
	private String genus;
	@JsonProperty(value = Accession.SPECIES)
	private String species;
	@JsonProperty(value = Accession.SPAUTHOR)
	private String spauthor;
	@JsonProperty(value = Accession.SUBTAXA)
	private String subtaxa;
	@JsonProperty(value = Accession.SUBTAUTHOR)
	private String subtauthor;
	@JsonProperty(value = Accession.UUID)
	private UUID uuid;
	@JsonProperty(value = Accession.ORIGCTY)
	private String orgCty;
	@JsonProperty(value = Accession.ACQDATE)
	private String acqDate;
	@JsonProperty(value = Accession.MLSSTAT)
	private Boolean mlsStat;
	@JsonProperty(value = Accession.INTRUST)
	private Boolean inTrust;
	@JsonProperty(value = Accession.AVAILABLE)
	private Boolean available;
	@JsonProperty(value = Accession.STORAGE)
	private Integer[] storage;
	@JsonProperty(value = Accession.SAMPSTAT)
	private Integer sampStat;
	@JsonProperty(value = Accession.DUPLSITE)
	private String[] duplSite;
	@JsonProperty(value = Accession.BREDCODE)
	private String bredCode;
	@JsonProperty(value = Accession.ANCEST)
	private String ancest;
	@JsonProperty(value = Accession.DONORCODE)
	private String donorCode;
	@JsonProperty(value = Accession.DONORNUMB)
	private String donorNumb;
	@JsonProperty(value = Accession.DONORNAME)
	private String donorName;
	@JsonProperty(value = Accession.COLL)
	private CollectingJson coll;
	@JsonProperty(value = Accession.GEO)
	private GeoJson geo;
	@JsonProperty(value = Accession.REMARKS)
	private Remark[] remarks;
	@JsonProperty(value = Accession.HISTORIC)
	private boolean historic;
	@JsonProperty(value = Accession.ACCEURL)
	private String acceUrl;

	public long getVersion() {
		return version;
	}

	public void setVersion(long l) {
		this.version = l;
	}

	public Long getGenesysId() {
		return genesysId;
	}

	public void setGenesysId(Long genesysId) {
		this.genesysId = genesysId;
	}

	public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getAcceNumb() {
		return acceNumb;
	}

	public void setAcceNumb(String acceNumb) {
		this.acceNumb = acceNumb;
	}

	public String getGenus() {
		return genus;
	}

	public void setGenus(String genus) {
		this.genus = genus;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getSpauthor() {
		return spauthor;
	}

	public void setSpauthor(String spauthor) {
		this.spauthor = spauthor;
	}

	public String getSubtaxa() {
		return subtaxa;
	}

	public void setSubtaxa(String subtaxa) {
		this.subtaxa = subtaxa;
	}

	public String getSubtauthor() {
		return subtauthor;
	}

	public void setSubtauthor(String subtauthor) {
		this.subtauthor = subtauthor;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getOrgCty() {
		return orgCty;
	}

	public void setOrgCty(String orgCty) {
		this.orgCty = orgCty;
	}

	public String getAcqDate() {
		return acqDate;
	}

	public void setAcqDate(String acqDate) {
		this.acqDate = acqDate;
	}

	public Boolean getMlsStat() {
		return mlsStat;
	}

	public void setMlsStat(Boolean mlsStat) {
		this.mlsStat = mlsStat;
	}

	public Boolean getInTrust() {
		return inTrust;
	}

	public void setInTrust(Boolean inTrust) {
		this.inTrust = inTrust;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	public Integer[] getStorage() {
		return storage;
	}

	public void setStorage(Integer[] storage) {
		this.storage = storage;
	}

	public Integer getSampStat() {
		return sampStat;
	}

	public void setSampStat(Integer sampStat) {
		this.sampStat = sampStat;
	}

	public String[] getDuplSite() {
		return duplSite;
	}

	public void setDuplSite(String[] duplSite) {
		this.duplSite = duplSite;
	}

	public String getBredCode() {
		return bredCode;
	}

	public void setBredCode(String bredCode) {
		this.bredCode = bredCode;
	}

	public String getAncest() {
		return ancest;
	}

	public void setAncest(String ancest) {
		this.ancest = ancest;
	}

	public String getDonorCode() {
		return donorCode;
	}

	public void setDonorCode(String donorCode) {
		this.donorCode = donorCode;
	}

	public String getDonorNumb() {
		return donorNumb;
	}

	public void setDonorNumb(String donorNumb) {
		this.donorNumb = donorNumb;
	}

	public String getDonorName() {
		return donorName;
	}

	public void setDonorName(String donorName) {
		this.donorName = donorName;
	}

	public CollectingJson getColl() {
		return coll;
	}

	public void setColl(CollectingJson coll) {
		this.coll = coll;
	}

	public GeoJson getGeo() {
		return geo;
	}

	public void setGeo(GeoJson geo) {
		this.geo = geo;
	}

	public Remark[] getRemarks() {
		return remarks;
	}

	public void setRemarks(Remark[] remarks) {
		this.remarks = remarks;
	}

	public void setHistoric(boolean historic) {
		this.historic = historic;
	}

	public boolean getHistoric() {
		return this.historic;
	}

	public String getAcceUrl() {
		return acceUrl;
	}

	public void setAcceUrl(String acceUrl) {
		this.acceUrl = acceUrl;
	}
}
