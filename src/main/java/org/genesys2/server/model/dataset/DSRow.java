package org.genesys2.server.model.dataset;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.genesys2.server.model.EntityId;

@Entity
@Table(name = "ds2row", uniqueConstraints = { @UniqueConstraint(columnNames = { "md5", "sha1" }) })
public class DSRow implements EntityId {

	@ManyToOne(fetch=FetchType.LAZY, optional = false)
	@JoinColumn(name="ds")
	private DS dataset;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "row", cascade = { CascadeType.REMOVE })
	private List<DSRowQualifier<?>> rowQualifiers;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "row")
	private List<DSValue<?>> values;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	@Column(columnDefinition = "binary(16)", updatable = false)
	private byte[] md5;

	@Column(columnDefinition = "binary(20)", updatable = false)
	private byte[] sha1;

	@Override
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DS getDataset() {
		return dataset;
	}
	
	public void setDataset(DS dataset) {
		this.dataset = dataset;
	}
	
	public List<DSRowQualifier<?>> getRowQualifiers() {
		return rowQualifiers;
	}

	public void setRowQualifiers(List<DSRowQualifier<?>> rowQualifiers) {
		this.rowQualifiers = rowQualifiers;
	}

	public List<DSValue<?>> getValues() {
		return values;
	}

	public void setValues(List<DSValue<?>> values) {
		this.values = values;
	}

	@Override
	public String toString() {
		return "DSR " + id;
	}

	public void setMd5(byte[] md5) {
		this.md5 = md5;
	}

	public byte[] getMd5() {
		return md5;
	}

	public void setSha1(byte[] sha1) {
		this.sha1 = sha1;
	}

	public byte[] getSha1() {
		return sha1;
	}
}
