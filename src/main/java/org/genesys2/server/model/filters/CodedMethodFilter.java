/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.filters;

import java.util.Collections;
import java.util.List;

public class CodedMethodFilter extends MethodFilter {
	private final List<ValueName<?>> options;

	public CodedMethodFilter(String name, String title, DataType dataType, List<ValueName<?>> options) {
		super(name, title, dataType, FilterType.LIST);
		this.options = Collections.unmodifiableList(options);
	}

	public List<ValueName<?>> getOptions() {
		return options;
	}
}