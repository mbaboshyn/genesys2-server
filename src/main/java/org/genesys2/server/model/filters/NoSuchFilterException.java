package org.genesys2.server.model.filters;

public class NoSuchFilterException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 336141773811831262L;

	public NoSuchFilterException(String message) {
		super(message);
	}

}
