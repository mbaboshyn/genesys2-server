/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.genesys2.server.model.AuditedModel;

/**
 * Current country status in the ITPGRFA.
 */
@Entity
@Table(name = "itpgrfa", uniqueConstraints = { @UniqueConstraint(name = "ITPGRFA_Country", columnNames = { "countryId" }) })
public class ITPGRFAStatus extends AuditedModel {

	private static final long serialVersionUID = 2766536505832914090L;

	/**
	 * Country
	 */
	@OneToOne(cascade = {}, optional = false)
	@JoinColumn(name = "countryId")
	private Country country;

	/**
	 * Contracting party
	 */
	@Column(name = "cp", length = 100)
	private String contractingParty;

	/**
	 * Membership
	 */
	@Column(name = "membership", length = 50)
	private String membership;

	/**
	 * Membership type
	 */
	@Column(name = "membershipBy", length = 50)
	private String membershipBy;

	/**
	 * Name of National Focal Point
	 */
	@Column(name = "nfp", length = 200)
	private String nameOfNFP;

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getContractingParty() {
		return contractingParty;
	}

	public void setContractingParty(String contractingParty) {
		this.contractingParty = contractingParty;
	}

	public String getMembership() {
		return membership;
	}

	public void setMembership(String membership) {
		this.membership = membership;
	}

	public String getMembershipBy() {
		return membershipBy;
	}

	public void setMembershipBy(String membershipBy) {
		this.membershipBy = membershipBy;
	}

	public String getNameOfNFP() {
		return nameOfNFP;
	}

	public void setNameOfNFP(String nameOfNFP) {
		this.nameOfNFP = nameOfNFP;
	}

}
