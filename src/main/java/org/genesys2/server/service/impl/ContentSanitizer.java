/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.HtmlSanitizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

@Component
public class ContentSanitizer {

	public static final Log LOG = LogFactory.getLog(ContentSanitizer.class);

	@Autowired
	private ContentService contentService;

	@Autowired
	private HtmlSanitizer htmlSanitizer;

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	public void sanitizeAll() {
		LOG.info("Sanitizing articles");

		Page<Article> articles;
		int page = 0;
		do {
			articles = contentService.listArticles(new PageRequest(page++, 10));

			for (final Article a : articles) {
				a.setBody(htmlSanitizer.sanitize(a.getBody()));
			}

			contentService.save(articles.getContent());

		} while (articles.hasNext());
	}
}
