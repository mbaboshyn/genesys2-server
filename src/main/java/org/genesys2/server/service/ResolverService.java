package org.genesys2.server.service;

import java.util.List;
import java.util.UUID;

import org.genesys2.server.model.IdUUID;
import org.genesys2.server.model.genesys.AccessionData;
import org.genesys2.server.model.genesys.AccessionId;
import org.genesys2.server.model.impl.FaoInstitute;

public interface ResolverService {

	IdUUID forward(UUID uuid);

	List<AccessionData> findMatches(FaoInstitute faoInstitute, String acceNumb);

}
