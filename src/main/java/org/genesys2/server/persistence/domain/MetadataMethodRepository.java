/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.List;

import org.genesys2.server.model.genesys.Metadata;
import org.genesys2.server.model.genesys.MetadataMethod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface MetadataMethodRepository extends JpaRepository<MetadataMethod, Long> {

	List<MetadataMethod> findByMetadata(Metadata metadata);

	@Query("select distinct mm.methodId from MetadataMethod mm where mm.metadata = ?1")
	List<Long> listMetadataMethods(Metadata metadata);

	@Query("select distinct mm.metadata from MetadataMethod mm where mm.methodId = ?1")
	List<Metadata> listMetadataByMethodId(long methodId);

	@Modifying
	@Query("delete from MetadataMethod mm where mm.metadata = ?1 and mm.methodId = ?2")
	int deleteByMetadataAndMethodId(Metadata metadata, long methodId);

	MetadataMethod findByMetadataAndMethodId(Metadata metadata, long methodId);

}
