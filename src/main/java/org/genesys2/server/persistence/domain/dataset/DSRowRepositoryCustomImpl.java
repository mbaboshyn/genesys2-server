package org.genesys2.server.persistence.domain.dataset;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.dataset.DS;
import org.genesys2.server.model.dataset.DSDescriptor;
import org.genesys2.server.model.dataset.DSQualifier;
import org.genesys2.server.model.dataset.DSRow;
import org.genesys2.server.model.dataset.DSValue;
import org.springframework.stereotype.Repository;

@Repository
public class DSRowRepositoryCustomImpl implements DSRowCustomRepository {
	public static final Log LOG = LogFactory.getLog(DSRowRepositoryCustomImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public DSRow findRow(DS ds, Object[] qualifiers) {
		LOG.info("QUAL: " + Arrays.toString(qualifiers));
		StringBuilder where = new StringBuilder();
		StringBuilder from = new StringBuilder();

		final int OFFSET = 1;
		int paramIdx = OFFSET;
		for (int i = 0; i < qualifiers.length; i++) {
			if (i > 0)
				from.append(", ");

			if (qualifiers[i] instanceof Long) {
				from.append("DSRowQualifierLong dsrq" + (i));
			} else if (qualifiers[i] instanceof Double) {
				from.append("DSRowQualifierDouble dsrq" + (i));
			} else if (qualifiers[i] instanceof String) {
				from.append("DSRowQualifierString dsrq" + (i));
			}

			if (i > 0) {
				where.append(" and dsrq" + (i - 1) + ".row = dsrq" + i + ".row and ");
			}
			where.append(" dsrq" + i + ".value = ?").append(paramIdx++).append(" and dsrq" + i + ".datasetQualifier = ?").append(paramIdx++);
		}

		LOG.info("select dsrq0.row from " + from + " where " + where.toString());
		Query q = entityManager.createQuery("select dsrq0.row from " + from + " where " + where.toString() + "");
		// q.setParameter(1, ds);

		paramIdx = OFFSET;
		for (int j = 0; j < qualifiers.length; j++) {
			q.setParameter(paramIdx++, qualifiers[j]);
			LOG.info("?" + (paramIdx - 1) + ": " + qualifiers[j]);
			q.setParameter(paramIdx++, ds.getQualifiers().get(j));
			LOG.info("?" + (paramIdx - 1) + ": " + ds.getQualifiers().get(j));
		}

		try {
			return (DSRow) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public Map<Long, DSValue<?>> rowValueMap(Collection<DSRow> dsrs, DSDescriptor dsd) {
		List<DSRow> validRows = new ArrayList<DSRow>(dsrs.size());
		for (DSRow dsr : dsrs) {
			if (dsr.getId() != null) {
				validRows.add(dsr);
			}
		}

		Map<Long, DSValue<?>> map = new HashMap<Long, DSValue<?>>();

		if (!validRows.isEmpty()) {
//			LOG.info("getting row value map");
			Query q = entityManager.createQuery("select dsv.row.id, dsv from DSValue dsv where dsv.row in (?1) and dsv.datasetDescriptor=?2");
			q.setParameter(1, validRows);
			q.setParameter(2, dsd);

			@SuppressWarnings("unchecked")
			List<Object[]> res = q.getResultList();

			for (Object[] r : res) {
				map.put((Long) r[0], (DSValue<?>) r[1]);
			}
		}

		return map;
	}

	/**
	 * Return row.id, dsdq1.value, dsdq2.value...
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getQualifiers(DS ds) {
		StringBuilder select = new StringBuilder();
		StringBuilder where = new StringBuilder();
		StringBuilder from = new StringBuilder();

		List<DSQualifier> dsq = ds.getQualifiers();
		Class<?> clazz;
		final int OFFSET = 1;
		int paramIdx = OFFSET;
		for (int i = 0; i < dsq.size(); i++) {
			if (i > 0)
				from.append(", ");

			// TODO Get from descriptor
			clazz = Long.class;

			if (clazz.equals(Long.class)) {
				from.append("DSRowQualifierLong dsrq").append(i);
			} else if (clazz.equals(Double.class)) {
				from.append("DSRowQualifierDouble dsrq").append(i);
			} else if (clazz.equals(String.class)) {
				from.append("DSRowQualifierString dsrq").append(i);
			}

			select.append(", ").append("dsrq").append(i).append(".value");

			if (i > 0) {
				where.append(" and dsrq" + (i - 1) + ".row = dsrq" + i + ".row and ");
			}
			where.append(" dsrq" + i + ".datasetQualifier = ?").append(paramIdx++);
		}

		LOG.info("select dsrq0.row from " + from + " where " + where.toString());
		Query q = entityManager.createQuery("select dsrq0.row.id " + select + " from " + from + " where " + where.toString() + "");

		paramIdx = OFFSET;
		for (int i = 0; i < dsq.size(); i++) {
			q.setParameter(paramIdx++, dsq);
		}

		return q.getResultList();
	}

	@Override
	public Object[] getRowValues(long rowId, Long[] columnDescriptors) {
		Query q = entityManager.createQuery("select dsrv.datasetDescriptor.id, dsrv from DSValue dsrv where dsrv.row.id = ?1");
		q.setParameter(1, rowId);

		// List of [dsd.id, dsrv]
		@SuppressWarnings("unchecked")
		List<Object[]> values = q.getResultList();

		Object[] result = new Object[columnDescriptors.length];
		for (Object[] v : values) {
			Long dsdId = (Long) v[0];

			for (int i = 0; i < columnDescriptors.length; i++) {
				if (columnDescriptors[i].equals(dsdId)) {
					result[i] = ((DSValue<?>) v[1]).getValue();
				}
			}
		}

		return result;
	}

	@Override
	public List<Object[]> getRowValues(List<Long> rowIds, Long[] datasetDescriptors) {
		Query q = entityManager.createQuery("select dsrv.datasetDescriptor.id, dsrv.row.id, dsrv from DSValue dsrv where dsrv.row.id in (:ids) and dsrv.datasetDescriptor.id in (:dsds)");
		q.setParameter("ids", rowIds);
		q.setParameter("dsds", Arrays.asList(datasetDescriptors));
//		LOG.info("Query ready");

		// Initialize results list with blank arrays
		List<Object[]> results = new ArrayList<Object[]>(rowIds.size());
		for (int i = 0; i < rowIds.size(); i++) {
			results.add(new Object[datasetDescriptors.length]);
		}
		List<Object[]> values = q.getResultList();

		// List of [dsd.id, dsrv]
		for (Object[] v : values) {
			// LOG.info("Got next");
			Long dsdId = (Long) v[0];
			Long rowId = (Long) v[1];
			DSValue<?> dsv = (DSValue<?>) v[2];
			entityManager.detach(dsv);
			
			Object[] result = results.get(rowIds.indexOf(rowId));
			for (int i = 0; i < datasetDescriptors.length; i++) {
				if (datasetDescriptors[i].equals(dsdId)) {
					result[i] = dsv.getValue();
				}
			}
		}

		return results;
	}
}
