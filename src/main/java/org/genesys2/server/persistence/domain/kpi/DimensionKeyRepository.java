/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain.kpi;

import java.util.List;

import org.genesys2.server.model.kpi.DimensionKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DimensionKeyRepository extends JpaRepository<DimensionKey, Long> {

	DimensionKey findByNameAndValue(String name, String value);

	List<DimensionKey> findByName(String name);

	@Query("select dk.value, dk from DimensionKey dk where dk.name = ?1")
	List<Object[]> loadMap(String name);

}
