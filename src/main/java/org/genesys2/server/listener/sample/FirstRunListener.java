/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.listener.sample;

import org.genesys2.server.listener.RunAsAdminListener;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.worker.InstituteUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

/**
 * Import country data and WIEWS institutes if none exist at startup
 *
 * @author mobreza
 */
@Component
public class FirstRunListener extends RunAsAdminListener {

	@Value("${auto.createContent}")
	private final boolean createContent = false;

	@Autowired
	GeoService geoService;

	@Autowired
	InstituteService instituteService;

	@Autowired
	InstituteUpdater instituteUpdater;

	@Override
	protected void init() throws Exception {
		if (!createContent) {
			_logger.info("Skipping content creation on startup.");
			return;
		}

		if (geoService.listAll().size() == 0) {
			_logger.info("No country data found. Loading");
			geoService.updateCountryData();
		}

		if (instituteService.list(new PageRequest(0, 1)).getTotalElements() == 0) {
			_logger.info("No WIEWS institutes found. Loading.");
			instituteUpdater.updateFaoInstitutes();
		}
	}

}
