/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.log4j.Logger;

public class LocaleWrappedServletRequest extends HttpServletRequestWrapper {
	private static final Logger LOG = Logger.getLogger(LocaleWrappedServletRequest.class);

	private final String remainingUrl;

	private String originalUrl;

	public LocaleWrappedServletRequest(HttpServletRequest request, String originalUrl, String remainingUrl) {
		super(request);
		this.originalUrl = originalUrl;
		this.remainingUrl = remainingUrl;
	}

	@Override
	public String getServletPath() {
		String servletPath = super.getServletPath();
		if (this.originalUrl.equals(servletPath)) {
			if (LOG.isDebugEnabled())
				LOG.debug("servletPath: " + servletPath + " remaining: " + remainingUrl);
			return remainingUrl;
		}
		return servletPath;
	}

	@Override
	public String getRequestURI() {
		String requestURI = super.getRequestURI();
		if (this.originalUrl.equals(requestURI)) {
			if (LOG.isDebugEnabled())
				LOG.debug("requestURI: " + requestURI + " remaining=" + remainingUrl);
			return remainingUrl;
		}
		return requestURI;
	}

}
