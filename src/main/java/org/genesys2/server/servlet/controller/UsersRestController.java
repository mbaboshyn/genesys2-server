/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.util.List;

import org.genesys2.server.exception.NotUniqueUserException;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.model.oauth.OAuthClientDetails;
import org.genesys2.server.service.OAuth2ClientDetailsService;
import org.genesys2.server.service.UserService;
import org.genesys2.server.servlet.model.UserList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * User REST-controller. It produces and consumes JSON. For "USER" role all
 * actions are read-only.
 *
 * @see org.genesys2.auth.common.service.UserService
 */
@Controller
public class UsersRestController extends BaseController {

	@Value("${base.url}")
	private String baseUrl;
	
	@Autowired
	protected UserService userService;
	
    @Autowired
    private OAuth2ClientDetailsService clientDetailsService;

    @RequestMapping(value = "/users", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public Object getUsers(@RequestParam(value = "startRow", required = false, defaultValue = "0") Integer startRow,
			@RequestParam(value = "pageSize", required = false, defaultValue = "0") Integer pageSize) throws UserException {
		return new UserList(userService.listWrapped(startRow, pageSize));
	}

	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public Object getUser(@PathVariable Long id) throws UserException {
		return userService.getWrappedById(id);
	}

	@RequestMapping(value = "/users", method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public void saveUser(@RequestBody @Validated User user) throws UserException {
		userService.addUser(user);
	}

	@RequestMapping(value = "/users", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public void updateUser(@RequestBody @Validated User user) throws UserException {
		try {
			userService.updateUser(user);
		} catch (final DataIntegrityViolationException e) { // for some reasons it's
														// not caught in service
			throw new NotUniqueUserException(e, user.getEmail());
		}
	}

	@RequestMapping(value = "/users",
	/* method = RequestMethod.DELETE) */
	// only GET, POST, PUT allowed
	method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@ResponseBody
	public void removeUser(@RequestParam("id") Long userId) throws UserException {
		userService.removeUserById(userId);
	}

    @RequestMapping(value = "/get_widget")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String getWidget(Model model,@RequestParam(value = "clientId",required = false) String clientId) {

        if (!clientId.equals("")){
            ClientDetails clientDetails= clientDetailsService.loadClientByClientId(clientId);
            String script =
                    "<script>(function(d, s, id) {\n" +
                            "var js, gjs = d.getElementsByTagName(s)[0];\n" +
                            "if (d.getElementById(id)) return;\n" +
                            "js = d.createElement(s); js.id = id;\n" +
                            "js.src = '"+baseUrl+"/webapi/genesys-webapi.js?client_id="+clientDetails.getClientId()+"&client_secret="+clientDetails.getClientSecret()+"';\n" +
                            "gjs.parentNode.insertBefore(js, gjs);\n" +
                            "}(document, 'script', 'genesys-api'));</script>";

            model.addAttribute("client", clientDetails);
            model.addAttribute("script", script);
        }

        List<OAuthClientDetails> clientDetailses = clientDetailsService.listClientDetails();
        model.addAttribute("clientDetails",clientDetailses);

        return "/user/widget";
    }
}
