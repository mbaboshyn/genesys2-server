/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import java.util.List;

import javax.xml.bind.ValidationException;

import net.sf.oval.ConstraintViolation;
import net.sf.oval.Validator;
import net.sf.oval.constraint.MatchPattern;
import net.sf.oval.constraint.MinLength;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import org.genesys2.server.exception.AuthorizationException;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.genesys.Parameter;
import org.genesys2.server.model.genesys.ParameterCategory;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.TraitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = { "/api/v0", "/json/v0" })
public class TraitsController extends RestController {

	@Autowired
	GenesysService genesysService;

	@Autowired
	TraitService traitService;

	@Autowired
	CropService cropService;

	@RequestMapping(value = "/descriptors", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object listTraits() throws AuthorizationException {
		LOG.info("Listing traits");
		final Page<Parameter> descriptors = traitService.listTraits(new PageRequest(0, 50));
		return OAuth2Cleanup.clean(descriptors);
	}

	@RequestMapping(value = "/descriptors/?{page}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object listTraits(@PathVariable("page") Integer page) throws AuthorizationException {
		LOG.info("Listing traits page " + page);
		final Page<Parameter> descriptors = traitService.listTraits(new PageRequest(page, 50));
		return OAuth2Cleanup.clean(descriptors);
	}

	@RequestMapping(value = "/methods", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object listMethods() throws AuthorizationException {
		LOG.info("Listing traits");
		final Page<Method> methods = traitService.listMethods(new PageRequest(0, 50));
		return OAuth2Cleanup.clean(methods);
	}

	@RequestMapping(value = "/methods/?{page}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object listMethods(@PathVariable("page") Integer page) throws AuthorizationException {
		LOG.info("Listing traits page " + page);
		final Page<Method> methods = traitService.listMethods(new PageRequest(page, 50));
		return OAuth2Cleanup.clean(methods);
	}

	@RequestMapping(value = "/mymethods", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object listMyMethods() {
		LOG.info("Listing user's methods");
		final List<Method> methods = traitService.listMyMethods();
		return OAuth2Cleanup.clean(methods);
	}

	@RequestMapping(value = "/categories", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object listCategories() {
		LOG.info("Listing parameter categories");
		final List<ParameterCategory> categories = traitService.listCategories();
		return OAuth2Cleanup.clean(categories);
	}

	/**
	 * Create a new crop property category (ParameterCategory) to use with
	 * descriptors (property/method/scale) meta-data
	 *
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "/category", method = { RequestMethod.PUT, RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object createCategory(@RequestBody CategoryJson categoryJson) throws ValidationException {

		LOG.info("Creating trait property category:");

		final Validator validator = new Validator();
		final List<ConstraintViolation> violations = validator.validate(categoryJson);
		if (violations.size() > 0) {
			// TODO We could do better messages on validation error
			throw new ModelValidationException("Validation problem", violations);
		}

		ParameterCategory category = traitService.getCategory(categoryJson.name);

		if (category == null) {
			// create if not available already?
			// Ignoring rdfUri for the time being
			// TODO: decide if adding rdfURI will be useful in the future?
			// category= traitService.addCategory(categoryJson.rdfUri,
			// categoryJson.name, categoryJson.i18n);
			category = traitService.addCategory(categoryJson.name, categoryJson.i18n);
		} else {
			LOG.warn("Property category " + categoryJson.name + " already exists?");
		}

		return category;
	}

	public static class CategoryJson {

		// Resource Description Framework (RDF)
		// Uniform Resource Identifier (URI)
		// Kept optional for now (maybe null or blank)
		public String rdfUri;

		@NotNull
		@NotBlank
		public String name;

		@NotNull
		@NotBlank
		public String i18n;

	}

	/**
	 * Adds a new property by crop, if such a property for a given crop and
	 * title doesn't already exist. If it does exist, then the existing record
	 * is retrieved and sent back.
	 *
	 * @param cropName
	 *            to which the property belongs (given as a string in the path
	 *            of the API call)
	 * @param propertyJson
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "/{crop}/property", method = { RequestMethod.PUT, RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object createProperty(@PathVariable("crop") String cropName, @RequestBody PropertyJson propertyJson) throws ValidationException {

		LOG.info("Creating property for crop '" + cropName + "':");

		final Validator validator = new Validator();
		final List<ConstraintViolation> violations = validator.validate(propertyJson);
		if (violations.size() > 0) {
			// TODO We could do better messages on validation error
			throw new ModelValidationException("Validation problem", violations);
		}

		LOG.info("Calling cropService.getCrop('" + cropName + "'):");

		final Crop crop = cropService.getCrop(cropName);

		LOG.info("Calling getParameter(" + propertyJson.rdfUri + ")");

		Parameter property;
		if (!(propertyJson.rdfUri == null || propertyJson.rdfUri.isEmpty())) {
			property = traitService.getParameter(propertyJson.rdfUri);
		} else {
			property = traitService.getParameter(crop, propertyJson.title);
		}

		LOG.info("Property found is " + (property != null ? property.getTitle() : "null") + ".");

		if (property == null) {

			LOG.info("Calling addParameter '" + propertyJson.title + "' in category '" + propertyJson.category + "'.");

			// create if not available already?
			property = traitService.addParameter(propertyJson.rdfUri, crop, propertyJson.category, propertyJson.title, propertyJson.i18n);

		}
		else {
			LOG.warn("Property '" + propertyJson.title + "' in category '" + propertyJson.category + "' for crop '" + cropName + "' already exists?");
		// TODO perhaps update parameter.i18n value here?
		}

		return property;
	}

	public static class PropertyJson {

		// Resource Description Framework (RDF)
		// Uniform Resource Identifier (URI)
		// Kept optional for now (maybe null or blank)
		public String rdfUri;

		@NotNull
		@NotBlank
		public String category;

		@NotNull
		@NotBlank
		public String title;

		@NotNull
		@NotBlank
		public String i18n;

	}

	/**
	 * Add a crop property method to the database.
	 *
	 * @return
	 * @throws ValidationException
	 */
	@RequestMapping(value = "/{propertyId}/method", method = { RequestMethod.PUT, RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object createMethod(@PathVariable("propertyId") long propertyId, @RequestBody MethodJson methodJson) throws ValidationException {
		LOG.info("Creating method for property:");

		final Validator validator = new Validator();
		final List<ConstraintViolation> violations = validator.validate(methodJson);
		if (violations.size() > 0) {
			// TODO We could do better messages on validation error
			throw new ModelValidationException("Validation problem", violations);
		}

		Parameter property;
		if (propertyId != 0) {
			property = traitService.getTrait(propertyId);
		} else {
			// if the (long integer) property id given in the path is zero,
			// then the method is left "floating" in the database.
			property = null;
		}

		Method method;
		if (methodJson.rdfUri != null && !methodJson.rdfUri.isEmpty()) {
			method = traitService.getMethod(methodJson.rdfUri);
		} else {
			method = traitService.getMethod(methodJson.description, property);
		}

		if (method == null) {
			// create if not available already?
			method = traitService.addMethod(methodJson.rdfUri, methodJson.description, methodJson.i18n, methodJson.unit, methodJson.fieldName,
					methodJson.fieldType, methodJson.fieldSize, methodJson.options, methodJson.range, property);
		} else if (property == null) {
			LOG.warn("Floating Method '" + methodJson.description + "'?");
		} else {
			LOG.warn("Method '" + methodJson.description + "' already exists for property '" + property.getTitle() + "'?");
		}

		return method;
	}

	public static class MethodJson {

		// Resource Description Framework (RDF)
		// Uniform Resource Identifier (URI)
		// Kept optional for now (maybe null or blank)
		public String rdfUri;

		@NotNull
		@NotBlank
		@MinLength(10)
		public String description;

		@NotNull
		@NotBlank
		public String i18n;

		@NotBlank
		public String unit;

		@NotNull
		@NotBlank
		@MatchPattern(pattern = { "[a-zA-Z0-9]+" })
		public String fieldName;

		public int fieldType;

		public Integer fieldSize;

		@NotBlank
		public String options;

		@NotBlank
		public String range;
	}

}