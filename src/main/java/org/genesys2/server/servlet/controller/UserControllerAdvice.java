/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class UserControllerAdvice extends BaseController {

	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ExceptionHandler(value = { AccessDeniedException.class })
	// @ResponseBody
	public ModelAndView handleAccessDeniedException(AccessDeniedException e) {
		final ModelAndView mav = new ModelAndView("/errors/error");
		mav.addObject("exception", e);
		return mav;
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(value = { ResourceNotFoundException.class })
	// @ResponseBody
	public ModelAndView handleResourceNotFoundException(ResourceNotFoundException e) {
		final ModelAndView mav = new ModelAndView("/errors/error");
		mav.addObject("exception", e);
		return mav;
	}

	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ExceptionHandler(value = { AuthenticationException.class })
	@ResponseBody
	public String handleAuthenticationException(AuthenticationException e) {
		return simpleExceptionHandler(e);
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = { Throwable.class })
	// @ResponseBody
	public ModelAndView handleAll(Throwable e) {
		_logger.error(e.getMessage(), e);
		final ModelAndView mav = new ModelAndView("/errors/error");
		mav.addObject("exception", e);
		return mav;
	}

	// do not pass validation
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = { MethodArgumentNotValidException.class })
	@ResponseBody
	public Object handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
		return transformErrors(e.getBindingResult());
	}

	private Map<String, String> transformErrors(Errors errors) {
		final Map<String, String> errorsMap = new HashMap<String, String>();

		final List<ObjectError> allErrors = errors.getAllErrors();

		// todo probably handle and values
		for (final ObjectError error : allErrors) {
			final String objectName = error instanceof FieldError ? ((FieldError) error).getField() : error.getObjectName();
			errorsMap.put(objectName, messageSource.getMessage(error.getDefaultMessage(), new Object[] { objectName }, getLocale()));
		}

		return errorsMap;
	}
}
