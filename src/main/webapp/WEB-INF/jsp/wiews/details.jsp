<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><c:out value="${faoInstitute.fullName}" /><c:if test="${faoInstitute.acronym != ''}"> | ${faoInstitute.acronym}</c:if>
  | <c:out value="${faoInstitute.code}" /></title>
<meta name="description"
  content="<spring:message code="faoInstitute.meta-description" arguments="${faoInstitute.fullName}|${faoInstitute.acronym}" argumentSeparator="|" />"
/>
<meta name="keywords" content="${faoInstitute.acronym}, ${faoInstitute.fullName}, ${faoInstitute.code}" />
</head>
<body typeof="schema:Organization">
  <h1>
    <img class="country-flag bigger"
      src="<c:url value="${cdnFlagsUrl}" />/${faoInstitute.country.code3.toUpperCase()}.png"
    /> <span property="schema:Organization#name"><c:out value="${faoInstitute.fullName}" /></span>
    <c:if test="${faoInstitute.acronym != ''}"> &mdash; ${faoInstitute.acronym}</c:if>
    <small><c:out value="${faoInstitute.code}" /></small>
  </h1>

  <c:if test="${not faoInstitute.current}">
    <div class="alert alert-warning">
      <spring:message code="faoInstitute.institute-not-current" />
      <a href="<c:url value="/wiews/${faoInstitute.vCode}" />"><spring:message
          code="faoInstitute.view-current-institute" arguments="${faoInstitute.vCode}"
        /></a>
    </div>
  </c:if>

  <c:if test="${faoInstitute.current and countByInstitute eq 0}">
    <div class="alert alert-info">
      <spring:message code="faoInstitute.no-accessions-registered" />
    </div>
  </c:if>

  <div class="jumbotron">
    <spring:message code="faoInstitutes.stat.accessionCount" arguments="${countByInstitute}" />
    <c:if test="${countByInstitute gt 0}">
      <a title="<spring:message code="faoInstitute.data-title" arguments="${faoInstitute.fullName}" />"
        href="<c:url value="/wiews/${faoInstitute.code}/data" />"
      ><spring:message code="view.accessions" /></a>
      <a title="<spring:message code="faoInstitute.overview-title" arguments="${faoInstitute.fullName}" />"
        href="<c:url value="/wiews/${faoInstitute.code}/overview" />"
      ><spring:message code="data-overview.short" /></a>
    </c:if>
    <br />
    <spring:message code="faoInstitutes.stat.datasetCount" arguments="${datasetCount}" />
    <c:if test="${datasetCount gt 0}">
      <spring:message code="statistics.phenotypic.stats-text" arguments="${statisticsPheno.elStats}" />
      <a title="<spring:message code="faoInstitute.datasets-title" arguments="${faoInstitute.fullName}" />"
        href="<c:url value="/wiews/${faoInstitute.code}/datasets" />"
      ><spring:message code="view.datasets" /></a>
    </c:if>
  </div>

  <div class="">
    <security:authorize access="hasRole('ADMINISTRATOR') or hasPermission(#faoInstitute, 'ADMINISTRATION')">
      <a
        href="<c:url value="/acl/${faoInstitute.getClass().name}/${faoInstitute.id}/permissions"><c:param name="back">/wiews/${faoInstitute.code}</c:param></c:url>"
        class="close"
      > <spring:message code="edit-acl" /></a>
      <a href="<c:url value="/wiews/${faoInstitute.code}/edit" />" class="close"> <spring:message code="edit" />
      </a>
    </security:authorize>

    <span property="schema:Organization#description"> <%@include
        file="/WEB-INF/jsp/content/include/blurp-display.jsp"
      %>
    </span>

    <div class="row" style="">
      <div class="col-sm-4" property="schema:Organization#location">
        <spring:message code="faoInstitute.country" />
        : <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="<c:url value="/geo" />"
          itemprop="url"
        > <span itemprop="title"><spring:message code="menu.countries" /></span>
        </a> › <span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"> <a
            href="<c:url value="/geo/${faoInstitute.country.code3}" />" itemprop="url"
          > <span itemprop="title">${faoInstitute.country.getName(pageContext.response.locale)}</span>
          </a>
        </span>
        </span>
      </div>
      <%-- <div class="col-sm-4">
			<spring:message code="faoInstitute.code" />:
			<c:out value="${faoInstitute.code}" />
		</div>
		<div class="col-sm-4">
			<spring:message code="faoInstitute.acronym" />:
			<c:out value="${faoInstitute.acronym}" />
		</div> --%>
    </div>

    <div class="row" style="">
      <%-- <div class="col-sm-4">
			<spring:message code="faoInstitute.email" />:
			<c:out value="${faoInstitute.email}" />
		</div> --%>
      <%-- 		<p>
			<c:out value="${faoInstitute.type}" />
		</p>
 --%>
      <div class="col-sm-12">
        <spring:message code="faoInstitute.url" />
        :
        <c:forEach items="${faoInstitute.safeUrls}" var="url">
          <a target="_blank" rel="nofollow" href="<c:out value="${url}" />"><span dir="ltr"><c:out
                value="${url}"
              /></span></a>
        </c:forEach>
      </div>

      <div class="col-sm-12">
        <a target="_blank" rel="nofollow"
          href="<c:url value="http://apps3.fao.org/wiews/institute.htm?i_l=EN&query_INVALIDINST=Y"><c:param name="query_INSTCODE" value="${faoInstitute.code}" /></c:url>"
        ><spring:message code="faoInstitute.wiewsLink" arguments="${faoInstitute.code}" /></a>
      </div>

    </div>

    <c:if test="${organizations.size() gt 0}">
      <div class="row" style="">
        <div class="col-sm-12">
          <spring:message code="faoInstitute.member-of-organizations-and-networks" />
          <c:forEach items="${organizations}" var="organization">
            <a href="<c:url value="/org/${organization.slug}" />"><span dir="ltr"><c:out
                  value="${organization.title}"
                /></span></a>
          </c:forEach>
        </div>
      </div>
    </c:if>
  </div>

  <c:if test="${faoInstitute.latitude ne null}">
    <div class="row" style="">
      <div class="col-sm-12">
        <div id="map" class="gis-map"></div>
        <span property="schema:Organization#location"><span typeof="schema:Place"><span
            property="schema:Place#geo"
          ><span typeof="schema:GeoCoordinates"> <span property="schema:GeoCoordinates#latitude">${faoInstitute.latitude}</span>,
                <span property="schema:GeoCoordinates#longitude">${faoInstitute.longitude}</span>
            </span></span></span></span>
      </div>
    </div>
  </c:if>

  <div class="row" style="margin-top: 2em;">
    <c:if test="${statisticsCrops ne null}">
      <div class="col-xs-12">
        <h4>
          <spring:message code="faoInstitute.stat-by-crop" />
        </h4>
        <local:term-result termResult="${statisticsCrops}" type="crop" />
      </div>
    </c:if>

    <div class="col-sm-6">
      <h4>
        <spring:message code="faoInstitute.stat-by-genus" arguments="${statisticsGenus.numberOfElements}" />
      </h4>

      <div class="chart chart-pie">
        <div id="chartStatsByGenus" style="height: 300px;"></div>
      </div>

      <ul class="funny-list statistics">
        <c:forEach items="${statisticsGenus.content}" var="stat" varStatus="status">
          <li class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}"><span class="stats-number"><fmt:formatNumber
                value="${stat[1]}"
              /></span> <a
            title="<spring:message code="faoInstitute.link-species-data" arguments="${faoInstitute.fullName}|${faoInstitute.acronym}|${stat[0]}" argumentSeparator="|" />"
            href="<c:url value="/wiews/${faoInstitute.code}/t/${stat[0]}" />"
          ><span dir="ltr" class="sci-name"><c:out value="${stat[0]}" /></span></a></li>
        </c:forEach>
      </ul>
    </div>

    <div class="col-sm-6">
      <h4>
        <spring:message code="faoInstitute.stat-by-species" arguments="${statisticsTaxonomy.numberOfElements}" />
      </h4>

      <div class="chart chart-pie">
        <div id="chartStatsBySpecies" style="height: 300px"></div>
      </div>

      <ul class="funny-list statistics">
        <c:forEach items="${statisticsTaxonomy.content}" var="stat" varStatus="status">
          <li class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}"><span class="stats-number"><fmt:formatNumber
                value="${stat[1]}"
              /></span> <a
            title="<spring:message code="faoInstitute.link-species-data" arguments="${faoInstitute.fullName}|${faoInstitute.acronym}|${stat[0].genus} ${stat[0].species}" argumentSeparator="|" />"
            href="<c:url value="/wiews/${faoInstitute.code}/t/${stat[0].genus}/${stat[0].species}" />"
          ><span dir="ltr" class="sci-name"><c:out value="${stat[0].taxonName}" /></span></a></li>
        </c:forEach>
      </ul>
    </div>

  </div>

  <security:authorize access="isAuthenticated()">
    <c:if test="${statisticsPDCI.count gt 0 and statisticsPDCI.count eq countByInstitute}">
      <h3>
        <spring:message code="accession.pdci" />
      </h3>
      <p>
        <spring:message code="accession.pdci.stats-text" arguments="${statisticsPDCI.elStats}" />
      </p>

      <div class="chart chart-histogram">
        <div id="chartPDCI" style="height: 200px"></div>
      </div>

      <p>
        <a href="<c:url value="/content/passport-data-completeness-index" />"><spring:message
            code="accession.pdci.about-link"
          /></a>
      </p>
      <%-- <c:forEach items="${statisticsPDCI.histogram}" var="item" varStatus="index">
      <div>${index.count}=${item}</div>
    </c:forEach> --%>
    </c:if>
  </security:authorize>

  <c:if test="${countByInstitute gt 0}">
    <form class="form-horizontal" method="post" action="/download/wiews/${faoInstitute.code}/download">
      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
      <div class="row" style="margin-top: 2em;">
        <div class="col-sm-12">
          <security:authorize access="isAuthenticated()">
            <button name="mcpd" class="btn btn-primary" type="submit">
              <spring:message code="filter.download-mcpd" />
            </button>
            <button name="pdci" class="btn btn-default" type="submit">
              <spring:message code="filter.download-pdci" />
            </button>
          </security:authorize>
          <button name="dwca" class="btn btn-default" type="submit">
            <spring:message code="metadata.download-dwca" />
          </button>
        </div>
      </div>
    </form>
  </c:if>

  <content tag="javascript"> <c:if test="${faoInstitute.latitude ne null}">
    <script type="text/javascript">
			jQuery(document).ready(function() {
				var map=GenesysMaps.map("${pageContext.response.locale.language}", $("#map"), {
					minZoom: 4,
					maxZoom: 6, /* WIEWS does not provide enough detail */
					center: new GenesysMaps.LatLng(${faoInstitute.latitude}, ${faoInstitute.longitude}), 
					markerTitle: "<spring:escapeBody javaScriptEscape="true">${faoInstitute.fullName}</spring:escapeBody>",
					scrollWheelZoom: false,
					touchZoom: false,
					dragging: false,
					doubleClickZoom: false,
					boxZoom: false
				});
			});
		</script>
  </c:if> <script type="text/javascript">
			<%@include file="/WEB-INF/jsp/wiews/ga.jsp"%>
			_pageDim = { institute: '${faoInstitute.code}' };
		</script> <script>
        jQuery(document).ready(function () {
            GenesysChart.chart("#chartStatsByGenus", "<c:url value="/wiews/${faoInstitute.code}/stat-genus" />", null, null, function(genus) { window.location=window.location.pathname + "/t/" + genus; });
            GenesysChart.chart("#chartStatsBySpecies", "<c:url value="/wiews/${faoInstitute.code}/stat-species" />", null, function(taxonomy) { return taxonomy.taxonName; }, function(taxonomy) { window.location=window.location.pathname + "/t/" + taxonomy.genus + "/" + taxonomy.species; });
<security:authorize access="isAuthenticated()">
<c:if test="${statisticsPDCI.count gt 0 and statisticsPDCI.count eq countByInstitute}">
            GenesysChart.histogram("#chartPDCI", <c:out value="${statisticsPDCI.histogramJson}" escapeXml="false" /> );
</c:if>
</security:authorize>
        });
    </script> </content>
</body>
</html>