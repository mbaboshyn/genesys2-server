<%@ page contentType="application/opensearchdescription+xml" pageEncoding="UTF-8" language="java" session="false"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib
	prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><?xml version="1.0" encoding="UTF-8"?>

<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/" xmlns:moz="http://www.mozilla.org/2006/browser/search/">
   <ShortName><spring:message code="page.home.title" /></ShortName>
   <LongName>Search global accession database Genesys PGR</LongName>
   <Description></Description>
   <Image height="16" width="16" type="image/x-icon"><c:out value="${baseUrl}" />/html/images/genesys16.ico</Image>
   <Image height="256" width="256" type="image/x-icon"><c:out value="${baseUrl}" />/favicon.ico</Image>
   <Image height="64" width="64" type="image/png"><c:out value="${baseUrl}" />/html/images/genesys64.png</Image>
   <Tags>accession, PGR, genesys</Tags>
   <Contact>helpdesk@genesys-pgr.org</Contact>
   <Query role="example" searchTerms="IRGC 1001" />
   <Url type="application/rss+xml" template="<c:out value="${baseUrl}" /><c:url value="/acn/opensearch" />?q={searchTerms}&amp;page={startPage?}"/>
   <Url type="application/x-suggestions+json" template="<c:out value="${baseUrl}" /><c:url value="/acn/opensearch-gossip" />?q={searchTerms}"/>
   <Url type="text/html" template="<c:out value="${baseUrl}" /><c:url value="/acn/search" />?q={searchTerms}&amp;page={startPage?}"/>
   
   <Attribution>See <c:out value="${baseUrl}" /><c:url value="/content/terms" /></Attribution>
   <SyndicationRight>limited</SyndicationRight>
   <AdultContent>false</AdultContent>
   <Language>${pageContext.response.locale.language}</Language>
   <OutputEncoding>UTF-8</OutputEncoding>
   <InputEncoding>UTF-8</InputEncoding>
   
   <moz:SearchForm><c:out value="${baseUrl}" /><c:url value="/acn/search" /></moz:SearchForm>
</OpenSearchDescription>
