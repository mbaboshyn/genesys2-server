<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>
<head>
<title><spring:message code="search.page.title" /></title>
</head>
<body>
	<h1><spring:message code="search.page.title" /></h1>
	
	<div class="main-col-header clearfix">
	<form class="" method="get" action="<c:url value="/search" />">
			<div class="input-group col-lg-3"><span class="input-group-btn"><input type="text" name="q" class="span3 form-control" value="<c:out value="${q}" />" /><input type="submit" value="<spring:message code="search.button.label" />" class="btn" /></span></div>
	</form>
	</div>

	<c:if test="${pagedData.totalElements eq 0}">
		<div class="alert alert-info"><spring:message code="search.no-results" /></div>
	</c:if>

	<c:if test="${pagedData ne null and pagedData.totalElements gt 0}">
	<div class="main-col-header clearfix">
		<div class="nav-header pull-left">
			<div class="results"><spring:message code="paged.totalElements" arguments="${pagedData.totalElements}" /></div>
			<div class="pagination">
				<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
				<a class="${pagedData.number eq 0 ? 'disabled' :''}" href="?q=${q}&amp;page=${pagedData.number eq 0 ? 1 : pagedData.number}"><spring:message code="pagination.previous-page" /></a> <a href="?q=${q}&amp;page=${pagedData.number + 2}"><spring:message code="pagination.next-page" /></a>
			</div>
		</div>
	</div>


		<ul class="funny-list">
			<c:forEach items="${pagedData.content}" var="searchResult" varStatus="status">
				<c:set value="${searchResult.getClass().simpleName}" var="clazz" />
				<c:if test="${clazz.contains('_$$_')}">
					<c:set var="clazz" value="${clazz.substring(0, clazz.indexOf('_$$_'))}" />
				</c:if>
			
				<li><small>${clazz} <b><fmt:formatNumber value="${status.count + (pagedData.number)*pagedData.size}" /></b>.</small> 
				<c:choose>
				<c:when test="${clazz eq 'Taxonomy'}">
					<a href="<c:url value="/acn/t/${searchResult.genus}/${searchResult.species}" />"><c:out value="${searchResult.taxonName}" /></a>
				</c:when>
				<c:when test="${clazz eq 'Organization'}">
					<a href="<c:url value="/org/${searchResult.slug}" />"><c:out value="${searchResult.slug}" /></a> <c:out value="${searchResult.title}" />
				</c:when>
				<c:when test="${clazz eq 'Accession'}">
					<a href="<c:url value="/acn/id/${searchResult.id}" />">${searchResult.accessionName}</a> ${searchResult.taxonomy.taxonName} ${searchResult.instituteCode}
				</c:when>
				<c:when test="${clazz eq 'AccessionAlias'}">
					<a href="<c:url value="/acn/id/${searchResult.accession.id}" />">${searchResult.name}</a> ${searchResult.accession.taxonomy.taxonName} ${searchResult.accession.instituteCode}
				</c:when>
				<c:when test="${clazz eq 'Crop'}">
					<a href="<c:url value="/c/${searchResult.shortName}/data" />">${searchResult.name}</a>
				</c:when>
				<c:when test="${clazz eq 'FaoInstitute'}">
					<a href="<c:url value="/wiews/${searchResult.code}" />">${searchResult.fullName}</a>
					${searchResult.code} ${searchResult.country.getName(pageContext.response.locale)}
				</c:when>
				<c:when test="${clazz eq 'Metadata'}">
					<a href="<c:url value="/data/view/${searchResult.id}" />">${searchResult.title}</a>
				</c:when>
				<c:when test="${clazz eq 'ActivityPost'}">
					${searchResult.title}
				</c:when>
				<c:when test="${clazz eq 'Country'}">
					<a href="<c:url value="/geo/${searchResult.code3}" />">${searchResult.getName(pageContext.response.locale)}</a>
				</c:when>
				<c:when test="${clazz eq 'Article'}">
					<a href="<c:url value="/content/${searchResult.slug}" />">${searchResult.title}</a>
				</c:when>
				<c:otherwise>
					${searchResult}
				</c:otherwise>
				</c:choose>
				</li>
				
				<c:remove var="clazz" />
			</c:forEach>
		</ul>
	</c:if>
</body>
</html>