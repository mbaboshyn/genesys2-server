<%@ tag description="Display pagination block" pageEncoding="UTF-8" %>
<%@ tag body-content="tagdependent" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="page" required="true" type="org.springframework.data.domain.Page" %>
<%@ attribute name="action" required="false" type="java.lang.String" %>
<%
	
%>
<form method="get" action="">
	<div class="results"><spring:message code="paged.totalElements" arguments="${page.totalElements}" /></div>
	<jsp:doBody/>
	<div class="paginate">
		<span class="">
			<spring:message code="paged.pageOfPages" arguments="${page.number+1},${page.totalPages}" />
		</span>
			<c:if test="${page.number eq 0}">
				<a><spring:message code="pagination.previous-page" /></a>
			</c:if>
			<c:if test="${page.number gt 0}">
				<a href="<spring:url value=""><spring:param name="page" value="${page.number eq 0 ? 1 : page.number}" /></spring:url>"><spring:message code="pagination.previous-page" /></a>
			</c:if>
	
			<input class="form-control" style="display: inline; max-width: 5em; text-align: center" type="text" name="page" placeholder="${page.number + 1}" />
	
			<c:if test="${page.number ge page.totalPages-1}">
				<a><spring:message code="pagination.next-page" /></a>
			</c:if>
			<c:if test="${page.number lt page.totalPages-1}">
				<a href="<spring:url value=""><spring:param name="page" value="${page.number+2}" /></spring:url>"><spring:message code="pagination.next-page" /></a>
			</c:if>
	</div>
</form>