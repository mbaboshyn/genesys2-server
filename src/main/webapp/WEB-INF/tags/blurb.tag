<%@ tag description="Display blurb" pageEncoding="UTF-8" %>
<%@ tag body-content="tagdependent" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="blurb" required="true" type="org.genesys2.server.model.impl.Article" %>
<%@ attribute name="title" required="false" type="java.lang.Boolean" %>
<%
	
%>
<div class="free-text blurp" dir="${blurb.lang=='fa' || blurb.lang=='ar' ? 'rtl' : 'ltr'}">
	<c:out value="${blurb.body}" escapeXml="false" />
</div>

<c:if test="${blurb!=null && blurb.lang != pageContext.response.locale.language}">
	<local:not-translated />
</c:if>
