
-- Add @Id, @Version to acq_collect, ...
ALTER TABLE `all_accnames` RENAME TO  `accnames` ;
ALTER TABLE `all_acq_breeding` RENAME TO  `acq_breeding` ;
ALTER TABLE `all_acq_collect` RENAME TO  `acq_collect` ;
ALTER TABLE `all_acq_exchange` RENAME TO  `acq_exchange` ;
ALTER TABLE `all_environment` RENAME TO  `environment` ;

ALTER TABLE `acq_breeding` ADD COLUMN `version` BIGINT NOT NULL DEFAULT 0,
 CHANGE COLUMN `ALIS_Id` `ALIS_Id` BIGINT NOT NULL  ,
 ADD COLUMN `id` BIGINT NOT NULL AUTO_INCREMENT  FIRST ,
 DROP PRIMARY KEY ,
 ADD PRIMARY KEY (`id`) ,
 ADD UNIQUE INDEX `ALIS_Id_UNIQUE` (`ALIS_Id` ASC) ;

ALTER TABLE `acq_collect` ADD COLUMN `version` BIGINT NOT NULL DEFAULT 0,
 CHANGE COLUMN `ALIS_Id` `ALIS_Id` BIGINT NOT NULL  ,
 ADD COLUMN `id` BIGINT NOT NULL AUTO_INCREMENT  FIRST ,
 DROP PRIMARY KEY ,
 ADD PRIMARY KEY (`id`) ,
 ADD UNIQUE INDEX `ALIS_Id_UNIQUE` (`ALIS_Id` ASC) ;
 
 ALTER TABLE `acq_exchange` ADD COLUMN `version` BIGINT NOT NULL DEFAULT 0,
 CHANGE COLUMN `ALIS_Id` `ALIS_Id` BIGINT NOT NULL  ,
 ADD COLUMN `id` BIGINT NOT NULL AUTO_INCREMENT  FIRST ,
 DROP PRIMARY KEY ,
 ADD PRIMARY KEY (`id`) ,
 ADD UNIQUE INDEX `ALIS_Id_UNIQUE` (`ALIS_Id` ASC) ;
 
 ALTER TABLE `environment` ADD COLUMN `version` BIGINT NOT NULL DEFAULT 0,
 CHANGE COLUMN `ALIS_Id` `ALIS_Id` BIGINT NOT NULL  ,
 ADD COLUMN `id` BIGINT NOT NULL AUTO_INCREMENT  FIRST ,
 DROP PRIMARY KEY ,
 ADD PRIMARY KEY (`id`) ,
 ADD UNIQUE INDEX `ALIS_Id_UNIQUE` (`ALIS_Id` ASC) ;
 
 ALTER TABLE `accnames` ADD COLUMN `version` BIGINT NOT NULL DEFAULT 0,
 CHANGE COLUMN `ALIS_Id` `ALIS_Id` BIGINT NOT NULL  ,
 ADD COLUMN `id` BIGINT NOT NULL AUTO_INCREMENT  FIRST ,
 DROP PRIMARY KEY ,
 ADD PRIMARY KEY (`id`) ,
 ADD UNIQUE INDEX `ALIS_Id_UNIQUE` (`ALIS_Id` ASC) ;


-- Create acq_geo table
create table acq_geo as select id, ALIS_Id, version, LatitudeD as lat, LongitudeD as lng, Altitude as alt from environment;

ALTER TABLE `acq_geo` CHANGE COLUMN `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT, modify column alt double null, ADD PRIMARY KEY (`id`) ;

-- alter table environment drop column LatitudeD, drop column LongitudeD, drop column Altitude;

ALTER TABLE `accession` DROP COLUMN `dataSource` , CHANGE COLUMN `ACC_Numb_HI` `ACC_Numb_HI` VARCHAR(36) NOT NULL COMMENT 'Accession Number at Holding Institute'  , CHANGE COLUMN `Taxon_Code` `Taxon_Code` BIGINT NOT NULL COMMENT 'Taxon Code'  , CHANGE COLUMN `Acquisition_Source` `Acquisition_Source` VARCHAR(3) NULL COMMENT 'Acquisition Source Code'  , CHANGE COLUMN `Acquisition_Date` `Acquisition_Date` VARCHAR(8) NULL COMMENT 'Acquisition Date'  , CHANGE COLUMN `Origin` `Origin` VARCHAR(3) NULL COMMENT 'Country of Origin ISO Code'  , CHANGE COLUMN `Dubl_Inst` `Dubl_Inst` VARCHAR(8) NULL COMMENT 'Duplicate Institute'  , CHANGE COLUMN `Sample_Status` `Sample_Status` VARCHAR(3) NULL COMMENT 'Sample Status Code'  , CHANGE COLUMN `Storage` `Storage` VARCHAR(12) NULL COMMENT 'Storage Type Code'  , CHANGE COLUMN `Genuss` `Genuss` VARCHAR(32) NOT NULL  ;

