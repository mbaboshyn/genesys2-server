create table verificationtoken (
	id bigint not null auto_increment primary key, 
	createdBy bigint null, createdDate datetime null, 
	lastModifiedBy bigint null, lastModifiedDate datetime null, 
	uuid varchar(36) not null, 
	secret varchar(4) not null, 
	purpose varchar(36) not null, 
	data varchar(120) null
);
